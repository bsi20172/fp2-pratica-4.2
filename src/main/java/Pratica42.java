/**
 *
 * @author nmessias
 */

import utfpr.ct.dainf.if62c.pratica.Elipse;
import utfpr.ct.dainf.if62c.pratica.Circulo;

public class Pratica42 {
   
    public static void main(String[] args) {
        Elipse e = new Elipse(2.0, 3.2);
        Circulo c = new Circulo(5.4);
        
        System.out.println("Elpise: Área: " + e.getArea() + " Perimetro: " + e.getPerimetro());
        System.out.println("Circulo: Área: " + c.getArea() + " Perimetro: " + c.getPerimetro());
    } 
}
