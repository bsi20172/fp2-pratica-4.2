/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

import java.io.Serializable;

/**
 *
 * @author nmessias
 */
public class Elipse implements FiguraComEixos, Serializable{
    private double area;
    private double perimetro;
    private double eixoMenor, eixoMaior;
    
    public Elipse(double eixoMenor, double eixoMaior) {
        this.eixoMenor = eixoMenor;
        this.eixoMaior = eixoMaior;
    }

    public double getArea() {
        return Math.PI * this.eixoMenor * this.eixoMaior;
    }

    public double getPerimetro() {
        return Math.PI * (3 * (eixoMenor + eixoMaior) - Math.sqrt((3 * eixoMenor + eixoMaior) * (eixoMenor + 3 * eixoMaior)));
    }

    @Override
    public double getEixoMenor() {
        return this.eixoMenor;
    }

    @Override
    public double getEixoMaior() {
        return this.eixoMaior;
    }

    @Override
    public String getNome() {
        return this.getClass().getSimpleName();
    }
}
